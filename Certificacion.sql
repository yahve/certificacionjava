-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 12-01-2017 a las 16:16:49
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Certificacion`
--
CREATE DATABASE IF NOT EXISTS `Certificacion` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `Certificacion`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id_pregunta` int(11) NOT NULL,
  `objetivo` int(11) NOT NULL,
  `tema` varchar(64) COLLATE utf8_bin NOT NULL,
  `dificultad` varchar(32) COLLATE utf8_bin NOT NULL,
  `enunciado` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta1` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta2` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta3` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta4` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta5` varchar(1024) COLLATE utf8_bin NOT NULL,
  `respuesta6` varchar(1024) COLLATE utf8_bin NOT NULL,
  `correctas` varchar(1024) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `test`
--

INSERT INTO `test` (`id_pregunta`, `objetivo`, `tema`, `dificultad`, `enunciado`, `respuesta1`, `respuesta2`, `respuesta3`, `respuesta4`, `respuesta5`, `respuesta6`, `correctas`) VALUES
(3, 1, '1', '1', '0003.png', 'The code compiles.', 'The code does not compile.', 'Exactly one variable is automatically initialized.', 'Exactly two variables are automatically initialized.', 'num is a class variable', 'integer is a class variable', '011100'),
(7, 1, '8', 'media', '0007.png', 'movie.title = "Office Space";', 'movie.rating = "R";', 'movie.length = 89;', 'movie.description = "Comedy";', 'None of the above', '', ''),
(25, 1, '1', '1', '0025.png', 'See you next year', 'Congratulations', 'See you next yearCongratulations', 'The code does not output any test.', 'The code will not compile because of line 4.', 'The code will not compile because of line 5.', ''),
(50, 1, '1', '1', '0050.png', '3/7/14 11:22 AM', '6/7/14 11:22 AM', '5/10/15 11:22 AM', '6/10/15 11:22 AM', 'The code does not compile.', 'A runtime exception is thrown.', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TestsRealizados`
--

DROP TABLE IF EXISTS `TestsRealizados`;
CREATE TABLE `TestsRealizados` (
  `id_usuario` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `preguntas_elegidas` varchar(2048) COLLATE utf8_bin NOT NULL,
  `respuestas_dadas` varchar(2048) COLLATE utf8_bin NOT NULL,
  `respuestas_correctas` varchar(2048) COLLATE utf8_bin NOT NULL,
  `puntuacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `TestsRealizados`
--

INSERT INTO `TestsRealizados` (`id_usuario`, `fecha`, `preguntas_elegidas`, `respuestas_dadas`, `respuestas_correctas`, `puntuacion`) VALUES
(1, '2017-01-03 02:12:21', '7,50,3,75,10', '001000,111000,000100,101000,110000', '001000,110100,100000,101000,110000', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `TestsRealizados`
--
ALTER TABLE `TestsRealizados`
  ADD PRIMARY KEY (`id_usuario`,`fecha`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_usuario_2` (`id_usuario`,`fecha`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `test`
--
ALTER TABLE `test`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
